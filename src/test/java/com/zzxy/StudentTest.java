package com.zzxy;

import com.zzxy.entity.DormitoryAdmin;
import com.zzxy.entity.Student;
import com.zzxy.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@ContextConfiguration(value = "classpath:applicationcontext.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class StudentTest {

    @Autowired
    private StudentService StudentService;

    @Test
    public void listTest(){
        List<Student> students = this.StudentService.findStudentList();
        students.forEach(System.out::println);
    }
    @Test
    public void localDateToStrTest(){
        LocalDate date = LocalDate.now();
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String dateStr = date.format(fmt);
        System.out.println("LocalDate转String:"+dateStr);
    }
    @Test
    public void updateTest(){
        // this.StudentService.modify(new Student(81,"31231","admin","男",39));
    }

}

