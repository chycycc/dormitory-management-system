package com.zzxy;

import com.zzxy.entity.DormitoryAdmin;
import com.zzxy.service.DormitoryAdminService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@ContextConfiguration(value = "classpath:applicationcontext.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class DormitoryAdminTest {

    @Autowired
    private DormitoryAdminService dormitoryAdminService;

    @Test
    public void listTest(){
        List<DormitoryAdmin> dormitoryAdminList = dormitoryAdminService.findDormitoryAdminList();
        dormitoryAdminList.forEach(System.out::println);
    }

}

