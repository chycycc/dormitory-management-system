package com.zzxy.dto;

import com.zzxy.entity.SystemAdmin;

public class SystemAdminDto {
    private int code;
    // private String msg;
    private SystemAdmin systemAdmin;

    public SystemAdminDto(int code, SystemAdmin systemAdmin) {
        this.code = code;
        this.systemAdmin = systemAdmin;
    }

    public SystemAdminDto() {

    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public SystemAdmin getSystemAdmin() {
        return systemAdmin;
    }

    public void setSystemAdmin(SystemAdmin systemAdmin) {
        this.systemAdmin = systemAdmin;
    }
}
