package com.zzxy.service;


import com.zzxy.entity.Student;
import com.zzxy.pojo.PageBean;

import java.util.List;

public interface StudentService {


    /**
     * 查找学生列表
     *
     * @return {@code List<Student>}
     */
    List<Student> findStudentList();

    /**
     * 搜索
     *
     * @param key      学号,姓名
     * @param value    值
     * @return {@code List<Student>}
     */
    List<Student> search(String key, String value);


    /**
     * 新增
     *
     * @param student 学生
     */
    void save(Student student);

    /**
     * 删除
     *
     * @param studentId   学生id
     * @param dormitoryId 宿舍编号
     */
    void remove(Integer studentId,Integer dormitoryId);

    /**
     * 修改
     *
     * @param student        学生
     * @param oldDormitoryId 旧宿舍ID
     */
    void modify(Student student,Integer oldDormitoryId);

    /**
     * 按宿舍 id 查找 学生list
     *
     * @param id 宿舍id
     * @return {@code List<Student>}
     */
    List<Student> findByDormitoryId(Integer id);

    /**
     * 分页
     *
     * @param page     页码
     * @param pageSize 页面大小
     * @return {@code PageBean}
     */
    PageBean page(Integer page, Integer pageSize);

    /**
     * 计数
     *
     * @return long
     */
    long count();
}
