package com.zzxy.service;

import com.zzxy.entity.Dormitory;
import com.zzxy.pojo.PageBean;

import java.util.List;

public interface DormitoryService {

    /**
     * 查找宿舍列表
     *
     * @return {@code List<Dormitory>}
     */
    List<Dormitory> findDormitoryList();

    /**
     * 查找空余宿舍
     *
     * @return {@code List<Dormitory>}
     */
    List<Dormitory> findFreeDormitoryList();

    /**
     * 新增宿舍
     *
     * @param dormitory 宿舍
     */
    void save(Dormitory dormitory);

    /**
     * 修改宿舍信息
     *
     * @param id        宿舍id
     * @param name      宿舍名
     * @param telephone 联系电话
     */
    void modify(Integer id, String name, String telephone);

    /**
     * 通过宿舍id删除宿舍
     *
     * @param id 编号
     */
    void remove(Integer id);

    /**
     * 搜索
     *
     * @param key   宿舍名,电话
     * @param value 值
     * @return {@code List<Dormitory>}
     */
    List<Dormitory> search(String key, String value);

    /**
     * 通过宿舍楼 id 查找宿舍
     *
     * @param buildingId 建筑物 ID
     * @return {@code List<Dormitory>}
     */
    List<Dormitory> findByBuildingId(Integer buildingId);

    /**
     * 分页
     *
     * @param page     页码
     * @param pageSize 页面大小
     * @return {@code PageBean}
     */
    PageBean page(Integer page, Integer pageSize);

    /**
     * 宿舍总数
     *
     * @return long
     */
    long count();
}
