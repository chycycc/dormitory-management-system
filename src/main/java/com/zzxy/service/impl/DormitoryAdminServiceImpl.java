package com.zzxy.service.impl;


import com.zzxy.dto.DormitoryAdminDto;
import com.zzxy.entity.DormitoryAdmin;
import com.zzxy.entity.Student;
import com.zzxy.mapper.DormitoryAdminMapper;
import com.zzxy.pojo.PageBean;
import com.zzxy.service.DormitoryAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DormitoryAdminServiceImpl implements DormitoryAdminService {

    @Autowired
    private DormitoryAdminMapper dormitoryAdminMapper;

    @Override
    public DormitoryAdminDto login(String username, String password) {
        DormitoryAdmin dormitoryAdmin = this.dormitoryAdminMapper.getByUsername(username);
        DormitoryAdminDto dormitoryAdminDto = new DormitoryAdminDto();

        if (dormitoryAdmin == null) {// 用户不存在,返回-1
            dormitoryAdminDto.setCode(-1);
        } else {// 查询到用户不为空,判断密码
            if (!dormitoryAdmin.getPassword().equals(password)) {// 密码错误,返回-2
                dormitoryAdminDto.setCode(-2);
            } else {
                // username和password都正确 ，返回结果
                dormitoryAdminDto.setDormitoryAdmin(dormitoryAdmin);
                dormitoryAdminDto.setCode(0);
            }
        }

        return dormitoryAdminDto;
    }

    @Override
    public List<DormitoryAdmin> findDormitoryAdminList() {
        return this.dormitoryAdminMapper.listDormitoryAdmins();
    }

    @Override
    public void save(DormitoryAdmin dormitoryAdmin) {
        this.dormitoryAdminMapper.insert(dormitoryAdmin);
    }

    @Override
    public void modify(DormitoryAdmin dormitoryAdmin) {
        this.dormitoryAdminMapper.update(dormitoryAdmin);
    }

    @Override
    public void remove(Integer id) {
        this.dormitoryAdminMapper.deleteById(id);
    }

    @Override
    public List<DormitoryAdmin> search(String key, String value) {
        return this.dormitoryAdminMapper.search(key,value);
    }

    @Override
    public PageBean page(Integer page, Integer pageSize) {
        // // 1.获取总记录数total
        Long count = this.dormitoryAdminMapper.count();
        // 2.获取数据列表
        Integer start = (page - 1) * pageSize;
        List<DormitoryAdmin> dormitoryAdminList = this.dormitoryAdminMapper.page(start, pageSize);
        // 3.封装PageBean对象
        PageBean pageBean = new PageBean(count,dormitoryAdminList);
        return pageBean;
    }

    @Override
    public long count() {
        return this.dormitoryAdminMapper.count();
    }
}
