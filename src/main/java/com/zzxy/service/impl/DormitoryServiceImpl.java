package com.zzxy.service.impl;


import com.zzxy.entity.Dormitory;
import com.zzxy.entity.DormitoryAdmin;
import com.zzxy.mapper.DormitoryMapper;
import com.zzxy.pojo.PageBean;
import com.zzxy.service.DormitoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DormitoryServiceImpl implements DormitoryService {

    @Autowired
    private DormitoryMapper dormitoryMapper;

    @Override
    public List<Dormitory> findDormitoryList() {
        return this.dormitoryMapper.listDormitories();
    }

    @Override
    public List<Dormitory> findFreeDormitoryList() {
        return this.dormitoryMapper.listFreeDormitories();
    }

    @Override
    public void save(Dormitory dormitory) {
        dormitory.setAvailable(dormitory.getType());
        this.dormitoryMapper.insertOne(dormitory);
    }

    @Override
    public void modify(Integer id, String name, String telephone) {
        this.dormitoryMapper.updateById(id,name,telephone);
    }

    @Override
    public void remove(Integer id) {
        this.dormitoryMapper.deleleById(id);
    }

    @Override
    public List<Dormitory> search(String key, String value) {
        return this.dormitoryMapper.search(key,value);
    }

    @Override
    public List<Dormitory> findByBuildingId(Integer buildingId) {
        return this.dormitoryMapper.listByBuildingId(buildingId);
    }

    @Override
    public PageBean page(Integer page, Integer pageSize) {
        // // 1.获取总记录数total
        Long count = this.dormitoryMapper.count();
        // 2.获取数据列表
        Integer start = (page - 1) * pageSize;
        List<Dormitory> dormitoryList = this.dormitoryMapper.page(start, pageSize);
        // 3.封装PageBean对象
        PageBean pageBean = new PageBean(count,dormitoryList);
        return pageBean;
    }

    @Override
    public long count() {
        return this.dormitoryMapper.count();
    }
}
