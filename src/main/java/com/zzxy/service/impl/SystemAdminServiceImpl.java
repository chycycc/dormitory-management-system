package com.zzxy.service.impl;


import com.zzxy.dto.SystemAdminDto;
import com.zzxy.entity.SystemAdmin;
import com.zzxy.mapper.SystemAdminMapper;
import com.zzxy.service.SystemAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemAdminServiceImpl implements SystemAdminService {

    @Autowired
    private SystemAdminMapper systemAdminMapper;

    @Override
    public SystemAdminDto login(String username, String password) {

        SystemAdmin systemAdmin = this.systemAdminMapper.getByUsername(username);
        SystemAdminDto systemAdminDto = new SystemAdminDto();

        if (systemAdmin == null) {// 用户不存在,返回-1
            systemAdminDto.setCode(-1);
        } else {// 查询到用户不为空,判断密码
            if (!systemAdmin.getPassword().equals(password)) {// 密码错误,返回-2
                systemAdminDto.setCode(-2);
            } else {
                // username和password都正确 ，返回结果
                systemAdminDto.setSystemAdmin(systemAdmin);
                systemAdminDto.setCode(0);
            }
        }

        return systemAdminDto;
    }
}
