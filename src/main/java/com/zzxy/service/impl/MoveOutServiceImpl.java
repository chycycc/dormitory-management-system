package com.zzxy.service.impl;


import com.zzxy.entity.Dormitory;
import com.zzxy.entity.Moveout;
import com.zzxy.entity.Student;
import com.zzxy.mapper.DormitoryMapper;
import com.zzxy.mapper.MoveOutMapper;
import com.zzxy.mapper.StudentMapper;
import com.zzxy.pojo.PageBean;
import com.zzxy.service.MoveOutService;
import com.zzxy.service.StudentService;
import com.zzxy.utils.convertDateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MoveOutServiceImpl implements MoveOutService {

    @Autowired
    private MoveOutMapper moveOutMapper;
    @Autowired
    private StudentMapper studentMapper;
    @Autowired
    private DormitoryMapper dormitoryMapper;
    @Override
    public List<Student> findStayingStudentList() {
        return this.moveOutMapper.listStayingStudents();
    }

    @Override
    public List<Student> search(String key, String value) {
        return this.moveOutMapper.search(key,value);
    }

    @Override
    public void save(Moveout moveout) {
        // 更新学生状态 入住---->迁出
        this.studentMapper.updateStateById(moveout.getStudentId());
        // 宿舍床位+1
        this.dormitoryMapper.addAvailable(moveout.getDormitoryId());
        // 迁出日期
        moveout.setCreateDate(convertDateUtil.DateToStr(new Date()));
        this.moveOutMapper.insert(moveout);
    }

    @Override
    public List<Moveout> findMoveOutStudentList() {
        return this.moveOutMapper.listMoveOutStudents();
    }

    @Override
    public List<Moveout> recordSearch(String key, String value) {
        if (key.equals("studentName") ){
            key = "s.name";
        } else if (key.equals("dormitoryName")){
            key = "d.name";
        }
        return this.moveOutMapper.recordSearch(key,value);
    }

    // @Override
    // public long count() {
    //     // return this.moveOutMapper.count();
    // }

    @Override
    public PageBean registerPage(Integer page, Integer pageSize) {
        // // 1.获取入住学生 总记录数total
        Long count = this.moveOutMapper.countStaying();
        // 2.获取数据列表
        Integer start = (page - 1) * pageSize;
        List<Student> stayingStudentList = this.moveOutMapper.registerPage(start, pageSize);
        // 3.封装PageBean对象
        PageBean pageBean = new PageBean(count,stayingStudentList);
        return pageBean;
    }

    @Override
    public PageBean recordPage(Integer page, Integer pageSize) {
        // // 1.获取迁出学生 总记录数total
        Long count = this.moveOutMapper.countMoveout();
        // 2.获取数据列表
        Integer start = (page - 1) * pageSize;
        List<Student> stayingStudentList = this.moveOutMapper.recordPage(start, pageSize);
        // 3.封装PageBean对象
        PageBean pageBean = new PageBean(count,stayingStudentList);
        return pageBean;
    }

    @Override
    public long countStaying() {
        return this.moveOutMapper.countStaying();
    }

    @Override
    public long countMoveout() {
        return this.moveOutMapper.countMoveout();
    }
}
