package com.zzxy.service.impl;


import com.zzxy.entity.Student;
import com.zzxy.mapper.DormitoryMapper;
import com.zzxy.mapper.StudentMapper;
import com.zzxy.pojo.PageBean;
import com.zzxy.service.StudentService;
import com.zzxy.utils.convertDateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentMapper studentMapper;
    @Autowired
    private DormitoryMapper dormitoryMapper;

    @Override
    public List<Student> findStudentList() {
        return this.studentMapper.listStudents();
    }

    @Override
    public List<Student> search(String key, String value) {
        return this.studentMapper.search(key,value);
    }

    @Override
    public void save(Student student) {
        String date = convertDateUtil.DateToStr(new Date());
        student.setState("入住");
        student.setCreateDate(date);
        Integer sub = this.dormitoryMapper.subAvailable(student.getDormitoryId());
        Integer save = this.studentMapper.insert(student);
        if (save != 1 || sub != 1) throw new RuntimeException("添加学生信息失败");
    }

    @Override
    public void remove(Integer studentId, Integer dormitoryId) {
        Integer delete = this.studentMapper.deleteById(studentId);
        Integer add = this.dormitoryMapper.addAvailable(dormitoryId);
        if (delete != 1 || add != 1) throw new RuntimeException("删除学生信息失败");
    }

    @Override
    public void modify(Student student, Integer oldDormitoryId) {
        Integer update = this.studentMapper.updateById(student);
        System.out.println(student);
        // 先增加旧宿舍床位
        Integer add = this.dormitoryMapper.addAvailable(oldDormitoryId);
        // 在减少新宿舍床位
        Integer sub = this.dormitoryMapper.subAvailable(student.getDormitoryId());
        if (update != 1 || add != 1 || sub != 1) throw new RuntimeException("添加学生信息失败");
    }

    @Override
    public List<Student> findByDormitoryId(Integer id) {
        return this.studentMapper.listBydormitoryId(id);
    }

    @Override
    public PageBean page(Integer page, Integer pageSize) {
        // // 1.获取总记录数total
        Long count = this.studentMapper.count();
        // 2.获取数据列表
        Integer start = (page - 1) * pageSize;
        List<Student> studentList = this.studentMapper.page(start, pageSize);
        // 3.封装PageBean对象
       PageBean pageBean = new PageBean(count,studentList);
        return pageBean;
    }

    @Override
    public long count() {
        return this.studentMapper.count();
    }
}
