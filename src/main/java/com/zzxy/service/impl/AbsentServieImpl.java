package com.zzxy.service.impl;


import com.zzxy.entity.Absent;
import com.zzxy.entity.Student;
import com.zzxy.mapper.AbsentMapper;
import com.zzxy.pojo.PageBean;
import com.zzxy.service.AbsentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AbsentServieImpl implements AbsentService {

    @Autowired
    private AbsentMapper absentMapper;

    @Override
    public List<Absent> findAbsentList() {
        return this.absentMapper.listAbsents();
    }

    @Override
    public List<Absent> search(String key, String value) {
        if (key.equals("buildingName")){
            key = "b.name";
        } else if (key.equals("dormitoryName")) {
            key = "d.name";
        }
        return this.absentMapper.search(key,value);
    }

    @Override
    public void save(Absent absent) {
        // absent.setCreateDate(c);
        this.absentMapper.insert(absent);
    }

    @Override
    public PageBean page(Integer page, Integer pageSize) {
        // // 1.获取入住学生 总记录数total
        Long count = this.absentMapper.count();
        // 2.获取数据列表
        Integer start = (page - 1) * pageSize;
        List<Absent> absentList = this.absentMapper.page(start, pageSize);
        // 3.封装PageBean对象
        PageBean pageBean = new PageBean(count,absentList);
        return pageBean;
    }

    @Override
    public long count() {
        return this.absentMapper.count();
    }
}
