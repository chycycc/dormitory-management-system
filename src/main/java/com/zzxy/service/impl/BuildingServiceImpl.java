package com.zzxy.service.impl;


import com.zzxy.entity.Building;
import com.zzxy.entity.Dormitory;
import com.zzxy.entity.Student;
import com.zzxy.mapper.BuildingMapper;
import com.zzxy.mapper.DormitoryMapper;
import com.zzxy.mapper.StudentMapper;
import com.zzxy.pojo.PageBean;
import com.zzxy.service.BuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BuildingServiceImpl implements BuildingService {

    @Autowired
    private BuildingMapper buildingMapper;

    @Autowired
    private DormitoryMapper dormitoryMapper;

    @Autowired
    private StudentMapper studentMapper;


    @Override
    public List<Building> findBuildingList() {
        return this.buildingMapper.listBuildings();
    }

    @Override
    public List<Building> search(String key, String value) {
        return this.buildingMapper.search(key,value);
    }

    @Override
    public void save(Building building) {
        Integer insert = this.buildingMapper.insert(building);
        if (insert != 1) throw new RuntimeException("添加宿舍楼错误");
    }

    @Override
    public void modify(Building building) {
        Integer update = this.buildingMapper.update(building);
        if (update != 1) throw new RuntimeException("添加宿舍楼错误");

    }

    @Override
    public void remove(Integer id) {
        // 1. 学生换宿舍
        // 1.1 根据宿舍楼id 获取 宿舍集合
        List<Dormitory> dormitoryList = this.dormitoryMapper.listByBuildingId(id);
        // 1.2 遍历宿舍
        for (Dormitory dormitory : dormitoryList) {
            // 1.3 根据宿舍id获取该宿舍下的学生集合
            Integer dormitoryId = dormitory.getId();
            List<Student> studentList = this.studentMapper.listBydormitoryId(dormitoryId);
            // 1.4 遍历学生集合
            for (Student student : studentList) {
                Integer studentId = student.getId();
                // 1.5 查找其他宿舍楼空余宿舍 获取空余宿舍id
                Integer availableId = this.dormitoryMapper.getAvailableId();
                // 1.6 把学生放入空余宿舍
                Integer updateDormitory = this.studentMapper.updateDormitory(studentId, availableId);
                // 1.7 空余宿舍空床位-1
                Integer subAvailable = this.dormitoryMapper.subAvailable(availableId);
                // 1.8 判断失败
                if (updateDormitory != 1 || subAvailable != 1) throw new RuntimeException("学生更换宿舍失败");
            }
        }

        // 2.删除宿舍
        for (Dormitory dormitory : dormitoryList) {
            Integer dormitoryId = dormitory.getId();
            Integer delete = this.dormitoryMapper.deleteByDormitoryId(dormitoryId);
            if (delete != 1) throw new RuntimeException("宿舍信息删除失败");
        }
        // 3.删除宿舍楼
        Integer delete = this.buildingMapper.delete(id);
        if (delete != 1) throw new RuntimeException("宿舍楼删除失败");
    }

    @Override
    public PageBean page(Integer page, Integer pageSize) {
        // // 1.获取总记录数total
        Long count = this.buildingMapper.count();
        // 2.获取数据列表
        Integer start = (page - 1) * pageSize;
        List<Building> buildingList = this.buildingMapper.page(start, pageSize);
        // 3.封装PageBean对象
        PageBean pageBean = new PageBean(count,buildingList);
        return pageBean;
    }

    @Override
    public long count() {
        return this.buildingMapper.count();
    }
}
