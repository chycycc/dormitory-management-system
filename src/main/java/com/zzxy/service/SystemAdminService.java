package com.zzxy.service;

import com.zzxy.dto.SystemAdminDto;


public interface SystemAdminService {
    /**
     * 系统管理员登录
     *
     * @param username 用户名
     * @param password 密码
     * @return {@code SystemAdminDto}
     */
    public SystemAdminDto login(String username, String password);
}
