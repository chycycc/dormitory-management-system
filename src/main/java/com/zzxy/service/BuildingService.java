package com.zzxy.service;

import com.zzxy.entity.Building;
import com.zzxy.pojo.PageBean;

import java.util.List;

public interface BuildingService {

    /**
     *  查找宿舍楼列表
     *
     * @return {@code List<Building>}
     */
    List<Building> findBuildingList();
    /**
     *  搜索
     *
     * @param key   楼名,介绍
     * @param value 值
     * @return {@code List<Building>}
     */
    List<Building> search(String key,String value);

    /**
     * 新增
     *
     * @param building 宿舍楼
     */
    void save(Building building);

    /**
     * 修改
     *
     * @param building 宿舍楼
     */
    void modify(Building building);

    /**
     * 删除
     *
     * @param id 宿舍楼id
     */
    void remove(Integer id);
    /**
     * 分页
     *
     * @param page     页码
     * @param pageSize 页面大小
     * @return {@code PageBean}
     */
    PageBean page(Integer page, Integer pageSize);

    /**
     * 宿舍楼总数
     *
     * @return long
     */
    long count();
}
