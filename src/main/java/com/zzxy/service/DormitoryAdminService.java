package com.zzxy.service;

import com.zzxy.dto.DormitoryAdminDto;
import com.zzxy.entity.DormitoryAdmin;
import com.zzxy.pojo.PageBean;

import java.util.List;

public interface DormitoryAdminService {

    /**
     * 宿舍管理员登录
     *
     * @param username 用户名
     * @param password 密码
     * @return {@code DormitoryAdminDto}
     */
    DormitoryAdminDto login(String username, String password);

    /**
     * 查找宿舍管理员列表
     *
     * @return {@code List<DormitoryAdmin>}
     */
    List<DormitoryAdmin> findDormitoryAdminList();

    /**
     * 新增
     *
     * @param dormitoryAdmin 宿舍管理员
     */
    void save(DormitoryAdmin dormitoryAdmin);

    /**
     * 修改
     *
     * @param dormitoryAdmin 宿舍管理员
     */
    void modify(DormitoryAdmin dormitoryAdmin);

    /**
     * 删除
     *
     * @param id 编号
     */
    void remove(Integer id);

    List<DormitoryAdmin> search(String key, String value);

    /**
     * 分页
     *
     * @param page     页码
     * @param pageSize 页面大小
     * @return {@code PageBean}
     */
    PageBean page(Integer page, Integer pageSize);

    /**
     * 宿管总数
     *
     * @return long
     */
    long count();
}
