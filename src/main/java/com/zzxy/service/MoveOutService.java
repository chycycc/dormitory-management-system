package com.zzxy.service;


import com.zzxy.entity.Moveout;
import com.zzxy.entity.Student;
import com.zzxy.pojo.PageBean;

import java.util.List;

public interface MoveOutService {


    /**
     * 查找入住状态学生
     *
     * @return {@code List<Student>}
     */
    List<Student> findStayingStudentList();

    /**
     * 入住学生搜索
     *
     * @param key   学号,姓名
     * @param value 值
     * @return {@code List<Student>}
     */
    List<Student> search(String key, String value);

    /**
     * 迁出登记
     *
     * @param moveout 迁出学生
     */
    void save(Moveout moveout);

    /**
     * 查找迁出状态学生
     *
     * @return {@code List<Moveout>}
     */
    List<Moveout> findMoveOutStudentList();

    /**
     * 迁出学生记录搜索
     *
     * @param key   学生姓名,宿舍名
     * @param value 值
     * @return {@code List<Moveout>}
     */
    List<Moveout> recordSearch(String key, String value);

    /**
     * 学生迁出登记分页
     *
     * @param page     页码
     * @param pageSize 页面大小
     * @return {@code PageBean}
     */
    PageBean registerPage(Integer page, Integer pageSize);

    /**
     * 学生迁出记录分页
     *
     * @param page     页码
     * @param pageSize 页面大小
     * @return {@code PageBean}
     */
    PageBean recordPage(Integer page, Integer pageSize);

    /**
     * 入住学生总数
     *
     * @return long
     */
    long countStaying();

    /**
     * 迁出学生总数
     *
     * @return long
     */
    long countMoveout();
}
