package com.zzxy.service;

import com.zzxy.entity.Absent;
import com.zzxy.pojo.PageBean;

import java.util.List;

public interface AbsentService {

    /**
     * 查找学生缺寝
     *
     * @return {@code List<Absent>}
     */
    List<Absent> findAbsentList();

    /**
     * 缺寝记录搜索
     *
     * @param key   宿舍楼名,宿舍名
     * @param value 值
     * @return {@code List<Absent>}
     */
    List<Absent> search(String key, String value);

    /**
     * 新增学生缺勤记录
     *
     * @param absent 缺席
     */
    void save(Absent absent);

    /**
     * 分页
     *
     * @param page     页码
     * @param pageSize 页面大小
     * @return {@code PageBean}
     */
    PageBean page(Integer page, Integer pageSize);

    /**
     * 缺寝学生总数
     *
     * @return long
     */
    long count();
}
