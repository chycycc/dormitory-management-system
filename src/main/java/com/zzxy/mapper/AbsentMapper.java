package com.zzxy.mapper;

import com.zzxy.entity.Absent;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AbsentMapper {
    /**
     * 缺寝学生list
     *
     * @return {@code List<Absent>}
     */
    List<Absent> listAbsents();

    /**
     * 缺寝记录搜索
     *
     * @param key   宿舍楼名,宿舍名
     * @param value 值
     * @return {@code List<Absent>}
     */
    List<Absent> search(@Param("key") String key, @Param("value") String value);

    /**
     * 插入
     *
     * @param absent 缺寝学生
     */
    @Insert("insert into absent (building_id, dormitory_id, student_id, dormitory_admin_id, create_date, reason)" +
            " values (#{buildingId},#{dormitoryId},#{studentId},#{dormitoryAdminId},#{createDate},#{reason})")
    void insert(Absent absent);

    /**
     * 缺寝学生计数
     *
     * @return {@code Long}
     */
    @Select("select count(*) from absent")
    Long count();

    /**
     * 分页
     *
     * @param start    开始
     * @param pageSize 页面大小
     * @return {@code List<Absent>}
     */
    List<Absent> page(@Param("start") Integer start, @Param("pageSize") Integer pageSize);
}
