package com.zzxy.mapper;

import com.zzxy.entity.Building;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BuildingMapper {
    /**
     * 宿舍楼列表
     *
     * @return {@code List<Building>}
     */
    List<Building> listBuildings();

    /**
     * 搜索
     *
     * @param key    楼名,介绍
     * @param value 价值
     * @return {@code List<Building>}
     */
    List<Building> search(@Param("key") String key, @Param("value") String value);

    /**
     * 插入
     *
     * @param building 宿舍楼
     * @return {@code Integer}
     */
    @Insert("insert into building (name, introduction, admin_id) values (#{name},#{introduction},#{adminId})")
    Integer insert(Building building);

    /**
     * 更新
     *
     * @param building 宿舍楼
     * @return {@code Integer}
     */
    @Update("update building set name = #{name},introduction = #{introduction},admin_id = #{adminId} where id = #{id} ")
    Integer update(Building building);

    /**
     * 删除
     *
     * @param id 宿舍楼id
     * @return {@code Integer}
     */
    @Delete("delete from building where id = #{id}")
    Integer delete(Integer id);

    /**
     * 查询宿舍楼总数
     *
     * @return {@code Long}
     */
    @Select("select count(*) from building")
    Long count();

    /**
     * 分页
     *
     * @param start    开始
     * @param pageSize 页面大小
     * @return {@code List<Building>}
     */
    List<Building> page(@Param("start") Integer start,@Param("pageSize") Integer pageSize);
}
