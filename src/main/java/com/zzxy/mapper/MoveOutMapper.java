package com.zzxy.mapper;

import com.zzxy.entity.Moveout;
import com.zzxy.entity.Student;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface MoveOutMapper {
    /**
     * 入住学生
     *
     * @return {@code List<Moveout>}
     */
    List<Student> listStayingStudents();

    /**
     * 搜索
     *
     * @param key   学号,姓名
     * @param value 值
     * @return {@code List<Student>}
     */
    List<Student> search(@Param("key") String key,@Param("value") String value);

    /**
     * 插入迁出学生
     *
     * @param moveout 搬出
     */
    @Insert("insert into moveout (student_id, dormitory_id, reason, create_date)" +
            " values (#{studentId},#{dormitoryId},#{reason},#{createDate})")
    void insert(Moveout moveout);

    /**
     * 迁出学生列表
     *
     * @return {@code List<Moveout>}
     */
    List<Moveout> listMoveOutStudents();

    /**
     * 迁出学生记录搜索
     *
     * @param key   学生姓名,宿舍名
     * @param value 价值
     * @return {@code List<Moveout>}
     */
    List<Moveout> recordSearch(@Param("key") String key,@Param("value") String value);

    /**
     * 入住学生计数
     *
     * @return long
     */
    @Select("select count(*) from student where state = '入住'")
    long countStaying();

    /**
     * 迁出学生计数
     *
     * @return long
     */
    @Select("select count(*) from moveout")
    long countMoveout();

    /**
     * 学生迁出登记分页
     *
     * @param start    开始
     * @param pageSize 页面大小
     * @return {@code List<Student>}
     */
    List<Student> registerPage(@Param("start") Integer start, @Param("pageSize") Integer pageSize);

    /**
     * 学生迁出记录分页
     *
     * @param start    开始
     * @param pageSize 页面大小
     * @return {@code List<Student>}
     */
    List<Student> recordPage(@Param("start") Integer start, @Param("pageSize") Integer pageSize);
}
