package com.zzxy.mapper;

import com.zzxy.entity.Dormitory;
import com.zzxy.entity.DormitoryAdmin;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface DormitoryMapper {

    /**
     * 查询宿舍列表
     *
     * @return {@code List<Dormitory>}
     */
    List<Dormitory> listDormitories();


    /**
     * 查找空闲宿舍
     *
     * @return {@code List<Dormitory>}
     */
    @Select("select * from dormitory where available != 0")
    List<Dormitory> listFreeDormitories();

    /**
     * 按 ID 更新
     *
     * @param id 宿舍id
     * @return {@code Integer}
     */
    Integer updateById(@Param("id") Integer id,@Param("name") String name,@Param("telephone") String telephone);

    /**
     * 减少可用床位
     *
     * @param dormitoryId 宿舍编号
     * @return {@code Integer}
     */
    Integer subAvailable(Integer dormitoryId);

    /**
     * 添加可用床位
     *
     * @param dormitoryId 宿舍编号
     * @return {@code Integer}
     */
    @Update("update dormitory set available = available + 1 where id = #{dormitoryId}")
    Integer addAvailable(Integer dormitoryId);

    /**
     * 查询有剩余床位的宿舍id
     *
     * @return {@code Integer}
     */
    @Select("select id from dormitory  where available > 0 limit 0,1")
    Integer getAvailableId();

    /**
     * 按宿舍ID删除
     *
     * @param dormitoryId 宿舍id
     * @return {@code Integer}
     */
    @Delete("delete from dormitory where id = #{dormitoryId}")
    Integer deleteByDormitoryId(Integer dormitoryId);

    /**
     * 按宿舍楼 ID 查找宿舍
     *
     * @param id 宿舍楼id
     * @return {@code List<Dormitory>}
     */
    @Select("select * from dormitory where building_id = #{id}")
    List<Dormitory> listByBuildingId(Integer id);

    /**
     * 插入
     *
     * @param dormitory 宿舍
     */
    @Insert("insert into dormitory (building_id, name, type, available, telephone) " +
            "values (#{buildingId},#{name},#{type},#{available},#{telephone})")
    void insertOne(Dormitory dormitory);

    /**
     * 通过宿舍id删除
     *
     * @param id 编号
     */
    @Delete("delete from dormitory where id = #{id}")
    void deleleById(Integer id);

    /**
     * 搜索
     *
     * @param key   宿舍名,电话
     * @param value 值
     * @return {@code List<Dormitory>}
     */
    List<Dormitory> search(@Param("key") String key, @Param("value") String value);

    /**
     * 计数
     *
     * @return {@code Long}
     */
    @Select("select count(*) from dormitory")
    Long count();

    /**
     * 分页
     *
     * @param start    开始
     * @param pageSize 页面大小
     * @return {@code List<Dormitory>}
     */
    List<Dormitory> page(@Param("start") Integer start, @Param("pageSize") Integer pageSize);
}
