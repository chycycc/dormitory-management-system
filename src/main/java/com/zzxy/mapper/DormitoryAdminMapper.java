package com.zzxy.mapper;

import com.zzxy.entity.DormitoryAdmin;
import com.zzxy.entity.Student;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 宿舍管理员
 *
 * @version 1.0.0
 * @date 2023/12/19
 */
@Mapper
public interface DormitoryAdminMapper {
    /**
     * 通过用户名获取宿舍管理员
     *
     * @param username 用户名
     * @return {@code SystemAdmin}
     */
    @Select("select * from dormitory_admin where username = #{username}")
     DormitoryAdmin getByUsername(String username);

    /**
     * 获取宿舍管理员列表
     *
     * @return {@code List<DormitoryAdmin>}
     */
    // @Select("select * from dormitory_admin")
    List<DormitoryAdmin> listDormitoryAdmins();

    /**
     * 搜索
     *
     * @param key
     * @param value 值
     * @return {@code List<DormitoryAdmin>}
     */
    @Select("select * from dormitory_admin where ${key} like '%${value}%'")
    List<DormitoryAdmin> search(@Param("key") String key,@Param("value") String value);
    /**
     * 插入
     *
     * @param dormitoryAdmin 宿舍管理员
     */
    @Insert("insert into dormitory_admin (username, password, name, gender, telephone) " +
            "values (#{username},#{password},#{name},#{gender},#{telephone})")
    void insert(DormitoryAdmin dormitoryAdmin);

    /**
     * 更新
     *
     * @param dormitoryAdmin 宿舍管理员
     */
    @Update("update dormitory_admin " +
            "set username = #{username},password = #{password},name = #{name},gender = #{gender},telephone = #{telephone} " +
            "where id = #{id}")
    void update(DormitoryAdmin dormitoryAdmin);

    /**
     * 按 ID 删除
     *
     * @param id 编号
     */
    @Delete("delete from dormitory_admin where id = #{id};")
    void deleteById(Integer id);

    /**
     * 计数
     *
     * @return {@code Long}
     */
    @Select("select count(*) from dormitory_admin")
    Long count();

    /**
     * 页
     * 分页
     *
     * @param start    开始
     * @param pageSize 页面大小
     * @return {@code List<DormitoryAdmin>}
     */
    List<DormitoryAdmin> page(@Param("start") Integer start,@Param("pageSize") Integer pageSize);
}
