package com.zzxy.mapper;

import com.zzxy.entity.Student;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface StudentMapper {
    /**
     * 获得学生列表
     *
     * @return {@code List<Student>}
     */// @Select("select * from student")
    List<Student> listStudents();

    /**
     * 搜索
     *
     * @param key 学号,姓名
     * @param value 值
     * @return {@code List<Student>}
     */
    List<Student> search(@Param("key") String key, @Param("value") String value);


    /**
     * 插入
     *
     * @param student 学生
     * @return
     */
    @Insert("insert into student (number, name,gender , dormitory_id, state, create_date) " +
            "values (#{number},#{name},#{gender},#{dormitoryId},#{state},#{createDate})")
    Integer insert(Student student);

    /**
     * 按 ID 删除
     *
     * @param id 编号
     * @return
     */
    @Delete("delete from student where id = #{id};")
    Integer deleteById(Integer id);


    /**
     * 通过id获取学生
     *
     * @param id
     * @return {@code Student}
     */
    Student getById(Integer id);


    /**
     * 按 ID 更新
     *
     * @param student 学生
     * @return {@code Integer}
     */
    @Update("update student " +
            "set name = #{name},number = #{number},gender = #{gender},dormitory_id = #{dormitoryId} " +
            "where id = #{id}")
    Integer updateById(Student student);

    /**
     * 查询学生列表通过宿舍ID
     *
     * @param dormitoryId 宿舍编号
     * @return {@code List<Student>}
     */
    @Select("select * from student where dormitory_id = #{dormitoryId}")
    List<Student> listBydormitoryId(Integer dormitoryId);

    /**
     * 学生更新宿舍
     *
     * @param studentId   学生id
     * @param availableId 床位剩余的宿舍id
     * @return {@code Integer}
     */
    @Update("update student set dormitory_id = #{availableId} where id = #{studentId}")
    Integer updateDormitory(@Param(value = "studentId") Integer studentId,@Param(value = "availableId")Integer availableId);

    /**
     * 按 ID 更新状态
     *
     * @param studentId 学生id
     */
    @Update("update student set state = '迁出' where id = #{studentId}")
    void updateStateById(Integer studentId);


    /**
     * 查询总学生人数
     *
     * @return {@code Long}
     */
    @Select("select count(*) from student")
    Long count();

    /**
     * 分页
     *
     * @param start    起始
     * @param pageSize 页面大小
     * @return {@code List<Student>}
     */
    List<Student> page(@Param("start") Integer start,@Param("pageSize") Integer pageSize);
}
