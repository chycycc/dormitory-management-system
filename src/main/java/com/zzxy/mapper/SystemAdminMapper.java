package com.zzxy.mapper;

import com.zzxy.entity.SystemAdmin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


/**
 * 系统管理员Mapper
 *
 * @version 1.0.0
 * @date 2023/12/19
 */
@Mapper
public interface SystemAdminMapper {

    /**
     * 通过用户名获取系统管理员
     *
     * @param username 用户名
     * @return {@code SystemAdmin}
     */
    @Select("select * from system_admin where username = #{username}")
    SystemAdmin getByUsername(String username);
}
