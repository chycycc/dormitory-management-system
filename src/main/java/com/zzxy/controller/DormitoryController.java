package com.zzxy.controller;

import com.zzxy.entity.Building;
import com.zzxy.entity.Dormitory;
import com.zzxy.entity.DormitoryAdmin;
import com.zzxy.entity.Student;
import com.zzxy.pojo.PageBean;
import com.zzxy.service.BuildingService;
import com.zzxy.service.DormitoryService;
import com.zzxy.service.StudentService;

import com.zzxy.utils.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import net.sf.json.JSONArray;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 宿舍管理
 *
 * @author
 * @date 2023/12/19
 */
@Slf4j
@Controller
@RequestMapping("/dormitory")
public class DormitoryController {

    @Autowired
    private DormitoryService dormitoryService;
    @Autowired
    private BuildingService buildingService;
    @Autowired
    private StudentService studentService;
    /**
     * 宿舍列表
     *
     * @param model
     * @return {@code String}
     */
    @RequestMapping("/list")
    public String list(Model model){
        List<Dormitory> dormitoryList = this.dormitoryService.findDormitoryList();
        List<Building> buildingList = this.buildingService.findBuildingList();
        long total = this.dormitoryService.count();
        model.addAttribute("total",total);
        model.addAttribute("list", dormitoryList);
        model.addAttribute("buildingList", buildingList);
        return "/dormitorymanager";
    }

    /**
     * 宿舍分页
     *
     * @param page     页码
     * @param pageSize 页面大小
     * @param response 响应
     * @throws Exception 例外
     */
    @RequestMapping("/page")
    public void page(@RequestParam(defaultValue = "1") Integer page,
                     @RequestParam(defaultValue = "10") Integer pageSize,
                     HttpServletResponse response) throws Exception {
        PageBean pageBean = this.dormitoryService.page(page, pageSize);
        List<Building> buildingList = this.buildingService.findBuildingList();
        log.info("宿舍管理分页查询, 参数: ,{},{}", page, pageSize);
        Map<String,Object> map = new HashMap<>();
        map.put("buildingList",buildingList);
        map.put("pageBean",pageBean);
        JSONArray jsonArray = JSONArray.fromObject(map);
        ResponseUtil.write(response,jsonArray);
    }

    /**
     * 新增宿舍
     *
     * @param dormitory 宿舍
     * @return {@code String}
     */
    @RequestMapping("/save")
    public String save(Dormitory dormitory){
        this.dormitoryService.save(dormitory);
        return "redirect:/dormitory/list";
    }

    /**
     * 更新宿舍信息
     *
     * @param id        编号
     * @param name      名字
     * @param telephone 电话
     * @return {@code String}
     */
    @RequestMapping("/update")
    public String update(@RequestParam Integer id,
                         @RequestParam String name,
                         @RequestParam String telephone){
        this.dormitoryService.modify(id,name,telephone);
        return "redirect:/dormitory/list";
    }

    /**
     * 删除宿舍信息
     *
     * @param id 宿舍id
     * @return {@code String}
     */
    @RequestMapping("/delete")
    public String update(@RequestParam Integer id){
        this.dormitoryService.remove(id);
        return "redirect:/dormitory/list";
    }

    /**
     * 搜索
     *
     * @param key   宿舍名,电话
     * @param value 价值
     * @param model 型
     * @return {@code String}
     */
    @RequestMapping("/search")
    public String search( @RequestParam String key,
                          @RequestParam(defaultValue = "") String value,
                          Model model) {
        List<Dormitory> dormitoryList = this.dormitoryService.search(key,value);
        List<Building> buildingList = this.buildingService.findBuildingList();
        long total = this.dormitoryService.count();
        model.addAttribute("total",total);
        model.addAttribute("list", dormitoryList);
        model.addAttribute("buildingList", buildingList);
        return "/dormitorymanager";
    }

    /**
     * 按宿舍楼 id 查找 该宿舍楼下的宿舍和学生
     *
     * @param buildingId 宿舍楼id
     * @param response   响应
     * @throws IOException ioexception
     */
    @RequestMapping(value = "/findByBuildingId",name = "PO")
    public void findByBuildingId(@RequestParam Integer buildingId,
                                   HttpServletResponse response) throws IOException {
        System.out.println("buildingId=="+buildingId);
        List<Dormitory> dormitoryList = this.dormitoryService.findByBuildingId(buildingId);
        List<Student> studentList = this.studentService.findByDormitoryId(dormitoryList.get(0).getId());
        Map<String,List> map = new HashMap<>();
        map.put("dormitoryList",dormitoryList);
        map.put("studentList",studentList);
        JSONArray jsonArray = JSONArray.fromObject(map);
        response.setContentType("text/json;charset=UTF-8");
        response.getWriter().write(jsonArray.toString());

    }

}
