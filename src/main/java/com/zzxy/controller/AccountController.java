package com.zzxy.controller;

import com.zzxy.dto.DormitoryAdminDto;
import com.zzxy.dto.SystemAdminDto;
import com.zzxy.service.DormitoryAdminService;
import com.zzxy.service.SystemAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * 账户管理
 * 系统管理员和宿舍管理员
 *
 * @version 1.0.0
 * @date 2023/12/19
 */
@Controller
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private SystemAdminService systemAdminService;

    @Autowired
    private DormitoryAdminService dormitoryAdminService;

    /**
     * 登录
     *
     * @param username 用户名
     * @param password 密码
     * @param type     类型
     * @return {@code String}
     */
    @RequestMapping("/login")
    public String login(@RequestParam(value = "username") String username,
                        @RequestParam(value = "password") String password,
                        @RequestParam(value = "type") String type,
                        HttpServletRequest request,
                        Model model){
        System.out.println(username+"---"+password+"---"+type);

        switch (type){
            // 系统管理员
            case "systemAdmin":
                SystemAdminDto systemAdminDto = this.systemAdminService.login(username, password);
                // 判断code的值
                switch (systemAdminDto.getCode()){
                    case -1:
                        model.addAttribute("usernameError", "用户名不存在");
                        return "/login";
                    case -2:
                        model.addAttribute("passwordError", "密码错误");
                        return "/login";
                    case 0:
                        HttpSession session = request.getSession();
                        session.setAttribute("systemAdmin", systemAdminDto.getSystemAdmin());
                        // 重定向到系统管理员界面
                        return "redirect:/systemadmin.jsp";

                }
                break;
            // 宿舍管理员
            case "dormitoryAdmin":
                DormitoryAdminDto dormitoryAdminDto = this.dormitoryAdminService.login(username, password);
                switch (dormitoryAdminDto.getCode()){
                    case -1:
                        model.addAttribute("usernameError", "用户名不存在");
                        return "/login";
                    case -2:
                        model.addAttribute("passwordError", "密码错误");
                        return "/login";
                    case 0:
                        //跳转到登录成功界面
                        HttpSession session = request.getSession();
                        session.setAttribute("dormitoryAdmin", dormitoryAdminDto.getDormitoryAdmin());
                        // 重定向到宿舍管理员界面
                        return "redirect:/dormitoryadmin.jsp";
                }
                break;
        }

        return "/login";
    }

    @RequestMapping("/logout")
    public String logout(HttpServletRequest request){
        // 清除session
        request.getSession().invalidate();
        // 重定向到login界面
        return "redirect:/login.jsp";

    }

}
