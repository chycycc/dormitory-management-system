package com.zzxy.controller;

import com.zzxy.entity.Dormitory;
import com.zzxy.entity.Student;
import com.zzxy.pojo.PageBean;
import com.zzxy.service.DormitoryService;
import com.zzxy.service.StudentService;
import com.zzxy.utils.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 学生管理
 *
 * @author
 * @date 2023/12/19
 */
@Slf4j
@Controller
@RequestMapping("/student")
public class StudentConrroller {

    @Autowired
    private StudentService studentService;

    @Autowired
    private DormitoryService dormitoryService;

    /**
     *  学生列表
     *
     * @return {@code String}
     */
    @RequestMapping("/list")
    public String list(Model model) {
        List<Student> students = studentService.findStudentList();
        List<Dormitory> dormitorides = this.dormitoryService.findFreeDormitoryList();
        long total = this.studentService.count();
        model.addAttribute("dormitoryList", dormitorides);
        model.addAttribute("list", students);
        model.addAttribute("total",total);
        return "/studentmanager";
    }

    /**
     * 分页
     *
     * @param page     页码
     * @param pageSize 页面大小
     * @param response 响应
     * @throws Exception 异常
     */
    @RequestMapping("/page")
    public void page(@RequestParam(defaultValue = "1") Integer page,
                     @RequestParam(defaultValue = "10") Integer pageSize,
                     HttpServletResponse response) throws Exception {
        PageBean pageBean = this.studentService.page(page, pageSize);
        List<Dormitory> dormitoryList = this.dormitoryService.findFreeDormitoryList();
        log.info("学生分页查询, 参数: ,{},{}", page, pageSize);
        Map<String,Object> map = new HashMap<>();
        map.put("dormitoryList",dormitoryList);
        map.put("pageBean",pageBean);
        JSONArray jsonArray = JSONArray.fromObject(map);
        ResponseUtil.write(response,jsonArray);
    }

    /**
     * 新增
     *
     * @param dormitoryId 宿舍编号
     * @param number      学号
     * @param name        名字
     * @param gender      性别
     * @return {@code String}
     */
    @RequestMapping("/save")
    public String save(@RequestParam Integer dormitoryId,
                       @RequestParam String number,
                       @RequestParam String name,
                       @RequestParam String gender) {
        this.studentService.save(new Student(number, name, gender, dormitoryId));
        return "redirect:/student/list";
    }


    /**
     * 更新
     *
     * @param studentId      学生Id
     * @param number         学号
     * @param name           名字
     * @param gender         性别
     * @param dormitoryId    宿舍id
     * @param oldDormitoryId 旧宿舍ID
     * @return {@code String}
     */
    @RequestMapping("/update")
    public String update(@RequestParam(value = "id") Integer studentId,
                         @RequestParam String number,
                         @RequestParam String name,
                         @RequestParam String gender,
                         @RequestParam(value = "dormitoryId") Integer dormitoryId,
                         @RequestParam(value = "oldDormitoryId") Integer oldDormitoryId) {
        this.studentService.modify(new Student(studentId, number, name, gender, dormitoryId), oldDormitoryId);
        return "redirect:/student/list";
    }

    /**
     * 删除
     *
     * @param studentId   学生id
     * @param dormitoryId 宿舍id
     * @return {@code String}
     */
    @RequestMapping("/delete")
    public String delete(@RequestParam(value = "id") Integer studentId,
                         @RequestParam(value = "dormitoryId") Integer dormitoryId) {
        this.studentService.remove(studentId, dormitoryId);
        return "redirect:/student/list";
    }

    /**
     * 按宿舍ID查找学生
     *
     * @param dormitoryId 宿舍编号
     * @param response    响应
     * @throws IOException ioexception
     */
    @RequestMapping("/findByDormitoryId")
    public void findByDormitoryId(@RequestParam(value = "dormitoryId") Integer dormitoryId,
                                  HttpServletResponse response) throws IOException {
        List<Student> studentList = this.studentService.findByDormitoryId(dormitoryId);
        JSONArray jsonArray = JSONArray.fromObject(studentList);
        response.setContentType("text/json;charset=UTF-8");
        response.getWriter().write(jsonArray.toString());
    }


    /**
     * 搜索
     *
     * @param model 模型
     * @param key   姓名,学号
     * @param value 值
     * @return {@code String}
     */
    @RequestMapping("/search")
    public String search( @RequestParam(defaultValue = "name") String key,
                          @RequestParam(defaultValue = "") String value,
                          Model model) {
        List<Student> students = this.studentService.search(key,value);
        List<Dormitory> dormitorys = this.dormitoryService.findDormitoryList();
        long total = this.studentService.count();
        model.addAttribute("list", students);
        model.addAttribute("dormitoryList",dormitorys);
        model.addAttribute("total",total);
        return "/studentmanager";
    }
}
