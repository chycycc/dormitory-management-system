package com.zzxy.controller;

import com.zzxy.entity.Building;
import com.zzxy.entity.Moveout;
import com.zzxy.entity.Student;
import com.zzxy.pojo.PageBean;
import com.zzxy.service.MoveOutService;
import com.zzxy.utils.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.security.Key;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 学生迁出管理
 *
 * @author
 * @date 2023/12/19
 */
@Slf4j
@Controller
@RequestMapping("/moveout")
public class MoveOutController {

    @Autowired
    private MoveOutService moveOutService;

    /**
     * 入住学生列表
     *
     * @param model 型
     * @return {@code String}
     */
    @RequestMapping("/list")
    public String list(Model model) {
        List<Student> stayingStudentList = this.moveOutService.findStayingStudentList();
        long total = this.moveOutService.countStaying();
        model.addAttribute("total", total);
        model.addAttribute("list", stayingStudentList);
        return "/moveoutregister";
    }

    /**
     * 学生迁出登记分页
     *
     * @param page     页码
     * @param pageSize 页面大小
     * @param response 响应
     * @throws Exception 例外
     */
    @RequestMapping("/registerPage")
    public void registerPage(@RequestParam(defaultValue = "1") Integer page,
                             @RequestParam(defaultValue = "10") Integer pageSize,
                             HttpServletResponse response) throws Exception {
        PageBean pageBean = this.moveOutService.registerPage(page, pageSize);
        log.info("迁出登记分页查询, 参数: ,{},{}", page, pageSize);
        JSONArray jsonArray = JSONArray.fromObject(pageBean);
        ResponseUtil.write(response, jsonArray);
    }

    /**
     * 迁出记录分页
     *
     * @param page     页
     * @param pageSize 页面大小
     * @param response 响应
     * @throws Exception 例外
     */
    @RequestMapping("/recordPage")
    public void recordPage(@RequestParam(defaultValue = "1") Integer page,
                           @RequestParam(defaultValue = "10") Integer pageSize,
                           HttpServletResponse response) throws Exception {
        PageBean pageBean = this.moveOutService.recordPage(page, pageSize);
        log.info("迁出记录分页查询, 参数: ,{},{}", page, pageSize);
        JSONArray jsonArray = JSONArray.fromObject(pageBean);
        ResponseUtil.write(response, jsonArray);

    }

    /**
     * 入住学生搜索
     *
     * @param key   学号,姓名
     * @param value 值
     * @param model 型
     * @return {@code String}
     */
    @RequestMapping("/search")
    public String search(@RequestParam(defaultValue = "name") String key,
                         @RequestParam(defaultValue = "") String value,
                         Model model) {
        List<Student> stayingStudentList = this.moveOutService.search(key, value);
        long total = this.moveOutService.countStaying();
        model.addAttribute("total", total);
        model.addAttribute("list", stayingStudentList);
        return "/moveoutregister";
    }

    /**
     * 学生迁出登记 新增
     *
     * @param studentId   学生id
     * @param dormitoryId 宿舍id
     * @param reason      迁出原因
     * @param model       型
     * @return {@code String}
     */
    @RequestMapping("/moveout")
    public String moveout(Integer studentId, Integer dormitoryId, String reason, Model model) {
        this.moveOutService.save(new Moveout(studentId, dormitoryId, reason));
        return "redirect:/moveout/list";
    }

    /**
     * 学生迁出记录
     *
     * @param model 型
     * @return {@code String}
     */
    @RequestMapping("/record")
    public String record(Model model) {
        List<Moveout> moveoutList = this.moveOutService.findMoveOutStudentList();
        long total = this.moveOutService.countMoveout();
        model.addAttribute("total", total);
        model.addAttribute("list", moveoutList);
        return "/moveoutrecord";
    }

    /**
     * 学生迁出记录搜索
     *
     * @param key   学生姓名,宿舍名
     * @param value 值
     * @param model 型
     * @return {@code String}
     */
    @RequestMapping("/recordSearch")
    public String recordSearch(@RequestParam String key,
                               @RequestParam(defaultValue = "") String value,
                               Model model) {
        List<Moveout> moveoutStudentList = this.moveOutService.recordSearch(key, value);
        long total = this.moveOutService.countMoveout();
        model.addAttribute("total", total);
        model.addAttribute("list", moveoutStudentList);
        return "/moveoutrecord";
    }

}
