package com.zzxy.controller;

import com.zzxy.entity.Dormitory;
import com.zzxy.entity.DormitoryAdmin;
import com.zzxy.pojo.PageBean;
import com.zzxy.service.DormitoryAdminService;
import com.zzxy.utils.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 宿管管理
 *
 * @author
 * @date 2023/12/19
 */
@Slf4j
@Controller
@RequestMapping("/dormitoryAdmin")
public class DormitoryAdminController {

    @Autowired
    private DormitoryAdminService dormitoryAdminService;

    /**
     * 宿管列表
     *
     * @param model
     * @return {@code String}
     */
    @RequestMapping("/list")
    public String list(Model model) {
        List<DormitoryAdmin> dormitoryAdmins = this.dormitoryAdminService.findDormitoryAdminList();
        long total = this.dormitoryAdminService.count();
        model.addAttribute("total",total);
        model.addAttribute("list", dormitoryAdmins);
        return "/adminmanager";
    }

    /**
     * 宿管分页
     *
     * @param page     页码
     * @param pageSize 页面大小
     * @param response 响应
     * @throws Exception 例外
     */
    @RequestMapping("/page")
    public void page(@RequestParam(defaultValue = "1") Integer page,
                     @RequestParam(defaultValue = "10") Integer pageSize,
                     HttpServletResponse response) throws Exception {
        PageBean pageBean = this.dormitoryAdminService.page(page, pageSize);
        log.info("宿管管理分页查询, 参数: ,{},{}", page, pageSize);
        JSONArray jsonArray = JSONArray.fromObject(pageBean);
        ResponseUtil.write(response,jsonArray);
    }

    /**
     * 新增
     *
     * @param dormitoryAdmin 宿舍管理员
     * @return {@code String}
     */
    @RequestMapping("/save")
    public String save(DormitoryAdmin dormitoryAdmin) {
        this.dormitoryAdminService.save(dormitoryAdmin);
        return "redirect:/dormitoryAdmin/list";
    }

    /**
     * 更新
     *
     * @param dormitoryAdmin 宿舍管理员
     * @return {@code String}
     */
    @RequestMapping("/update")
    public String update(DormitoryAdmin dormitoryAdmin) {
        this.dormitoryAdminService.modify(dormitoryAdmin);
        return "redirect:/dormitoryAdmin/list";
    }


    /**
     * 删除
     *
     * @param id 管理员id
     * @return {@code String}
     */

    @RequestMapping("/delete")
    public String delete(Integer id) {
        this.dormitoryAdminService.remove(id);
        return "redirect:/dormitoryAdmin/list";
    }

    /**
     * 搜索
     *
     * @param key  用户名,姓名,电话
     * @param value 用户名,姓名,电话
     * @param model
     * @return {@code String}
     */
    @RequestMapping("/search")
    public String search( @RequestParam(defaultValue = "username") String key,
                          @RequestParam(defaultValue = "") String value,
                          Model model) {
        // System.out.println("key==="+key+"value==="+value);
        List<DormitoryAdmin> dormitoryAdmins = this.dormitoryAdminService.search(key,value);
        long total = this.dormitoryAdminService.count();
        model.addAttribute("total",total);
        model.addAttribute("list", dormitoryAdmins);
        return "/adminmanager";
    }


}
