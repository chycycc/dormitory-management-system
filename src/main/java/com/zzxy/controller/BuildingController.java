package com.zzxy.controller;

import com.zzxy.entity.Building;
import com.zzxy.entity.Dormitory;
import com.zzxy.entity.DormitoryAdmin;
import com.zzxy.entity.Student;
import com.zzxy.pojo.PageBean;
import com.zzxy.service.BuildingService;
import com.zzxy.service.DormitoryAdminService;
import com.zzxy.utils.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 楼宇管理
 *
 * @author
 * @date 2023/12/19
 */
@Slf4j
@Controller
@RequestMapping("/building")
public class BuildingController {


    @Autowired
    private BuildingService buildingService;
    @Autowired
    private DormitoryAdminService dormitoryAdminService;
    /**
     * 宿舍楼列表
     *
     * @param model
     * @return {@code String}
     */
    @RequestMapping("/list")
    public String list(Model model) {
        List<Building> buildings = this.buildingService.findBuildingList();
        List<DormitoryAdmin> dormitoryAdmins = this.dormitoryAdminService.findDormitoryAdminList();
        long total = this.buildingService.count();
        model.addAttribute("total",total);
        model.addAttribute("adminList",dormitoryAdmins);
        model.addAttribute("list",buildings);
        return "/buildingmanager";
    }

    /**
     * 宿舍楼分页
     *
     * @param page     页码
     * @param pageSize 页面大小
     * @param response 响应
     * @throws Exception 例外
     */
    @RequestMapping("/page")
    public void page(@RequestParam(defaultValue = "1") Integer page,
                     @RequestParam(defaultValue = "10") Integer pageSize,
                     HttpServletResponse response) throws Exception {
        PageBean pageBean = this.buildingService.page(page, pageSize);
        List<DormitoryAdmin> dormitoryAdmins = this.dormitoryAdminService.findDormitoryAdminList();
        log.info("楼宇管理分页查询, 参数: ,{},{}", page, pageSize);
        Map<String,Object> map = new HashMap<>();
        map.put("adminList",dormitoryAdmins);
        map.put("pageBean",pageBean);
        JSONArray jsonArray = JSONArray.fromObject(map);
        ResponseUtil.write(response,jsonArray);
    }

    /**
     * 搜索
     *
     * @param model 型
     * @param key   楼名,介绍
     * @param value 值
     * @return {@code String}
     */
    @RequestMapping(value = {"/search"})
    public String search(Model model,
                       @RequestParam(defaultValue = "name") String key,
                       @RequestParam(defaultValue = "") String value) {
        List<Building> buildings = this.buildingService.search(key,value);
        List<DormitoryAdmin> dormitoryAdmins = this.dormitoryAdminService.findDormitoryAdminList();
        long total = this.buildingService.count();
        model.addAttribute("total",total);
        model.addAttribute("adminList",dormitoryAdmins);
        model.addAttribute("list",buildings);
        return "/buildingmanager";
    }


    /**
     * 新增宿舍楼
     *
     * @param name         宿舍楼名
     * @param introduction 介绍
     * @param adminId      管理员id
     * @return {@code String}
     */
    @RequestMapping("/save")
    public String save(@RequestParam String name,
                       @RequestParam String introduction,
                       @RequestParam Integer adminId){
        this.buildingService.save(new Building(name,introduction,adminId));
        return "redirect:/building/list";
    }


    /**
     * 更新宿舍楼信息
     *
     * @param building 宿舍楼
     * @return {@code String}
     */
    @RequestMapping("/update")
    public String update(Building building){
        this.buildingService.modify(building);
        return "redirect:/building/list";
    }

    /**
     * 删除宿舍楼
     *
     * @param id 编号
     * @return {@code String}
     */
    @RequestMapping("/delete")
    public String delete(@RequestParam(value = "id") String id){
        this.buildingService.remove(Integer.parseInt(id));
        return "redirect:/building/list";
    }


}
