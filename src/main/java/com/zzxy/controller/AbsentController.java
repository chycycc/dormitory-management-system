package com.zzxy.controller;

import com.zzxy.entity.*;
import com.zzxy.pojo.PageBean;
import com.zzxy.service.AbsentService;
import com.zzxy.service.BuildingService;
import com.zzxy.service.DormitoryService;
import com.zzxy.service.StudentService;
import com.zzxy.utils.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 学生缺寝管理
 *
 * @author
 * @date 2023/12/19
 */
@Slf4j
@Controller
@RequestMapping("/absent")
public class AbsentController {

    @Autowired
    private AbsentService absentService;
    @Autowired
    private BuildingService buildingService;
    @Autowired
    private DormitoryService dormitoryService;
    @Autowired
    private StudentService studentService;

    /**
     * 学生缺勤记录
     *
     * @param model 型
     * @return {@code String}
     */
    @RequestMapping("/list")
    public String list(Model model){
        List<Absent> absentList = this.absentService.findAbsentList();
        long total = this.absentService.count();
        model.addAttribute("total",total);
        model.addAttribute("list",absentList);
        return "/absentrecord";
    }

    /**
     * 缺寝学生记录分页
     *
     * @param page     页码
     * @param pageSize 页面大小
     * @param response 响应
     * @throws Exception 例外
     */
    @RequestMapping("/page")
    public void page(@RequestParam(defaultValue = "1") Integer page,
                             @RequestParam(defaultValue = "10") Integer pageSize,
                             HttpServletResponse response) throws Exception {
        PageBean pageBean = this.absentService.page(page, pageSize);
        log.info("学生缺寝分页查询, 参数: ,{},{}", page, pageSize);
        JSONArray jsonArray = JSONArray.fromObject(pageBean);
        ResponseUtil.write(response, jsonArray);
    }


    /**
     * 搜索
     *
     * @param key   宿舍楼名,宿舍名
     * @param value 值
     * @param model 型
     * @return {@code String}
     */
    @RequestMapping("/search")
    public String search(@RequestParam String key,
                         @RequestParam(defaultValue = "") String value,
                         Model model){
        List<Absent> absentList = this.absentService.search(key,value);
        long total = this.absentService.count();
        model.addAttribute("total",total);
        model.addAttribute("list",absentList);
        return "/absentrecord";
    }

    /**
     * 初始化
     * 宿舍楼 宿舍 学生
     *
     * @param model 型
     * @return {@code String}
     */
    @RequestMapping("/init")
    public String init(Model model){
        List<Building> buildingList = this.buildingService.findBuildingList();
        List<Dormitory> dormitoryList = this.dormitoryService.findDormitoryList();
        List<Student> studentList = this.studentService.findStudentList();
        model.addAttribute("buildingList",buildingList);
        model.addAttribute("dormitoryList",dormitoryList);
        model.addAttribute("studentList",studentList);
        return "/absentregister";
    }

    @RequestMapping("/save")
    public String save(Absent absent, Model model, HttpServletRequest request){
        HttpSession session = request.getSession();
        DormitoryAdmin dormitoryAdmin = (DormitoryAdmin) session.getAttribute("dormitoryAdmin");
        absent.setDormitoryAdminId(dormitoryAdmin.getId());
        this.absentService.save(absent);
        return "redirect:/absent/init";
    }
}
