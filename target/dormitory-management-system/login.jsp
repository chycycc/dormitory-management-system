<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://www.jq22.com/jquery/bootstrap-4.2.1.css">
    <link rel="stylesheet" href="${baseurl}/css/style.default.css" id="theme-stylesheet">
    <c:set var="baseurl" value="${pageContext.request.contextPath}"/>
    <title>Document</title>
    <style>
        .msg {
            display: inline-block;
            position: absolute;
            bottom: -25px;
            left: 0;
        }
        /*.position-ab {*/
        /*    position: absolute;*/
        /*    bottom: -25px;*/
        /*    left: 0;*/
        /*}*/

    </style>
</head>

<body>
<div class="page login-page">
    <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
            <div class="row">
                <!-- Logo & Information Panel-->
                <div class="col-lg-6">
                    <div class="info d-flex align-items-center">
                        <div class="content">
                            <div class="logo">
                                <h1 class="display-1">欢迎登录</h1>
                            </div>
                            <h1>DORMS宿舍管理系统</h1>
                        </div>
                    </div>
                </div>
                <!-- Form Panel    -->
                <div class="col-lg-6 bg-white">
                    <div class="form d-flex align-items-center">
                        <div class="content">
                            <form method="post" class="form-validate" id="login_form" action="<%=request.getContextPath()%>/account/login" >
                                <div class="form-group">
                                    <input id="login-username" type="text" name="username" autofocus
                                           data-msg="请输入用户名" placeholder="用户名" class="input-material" autocomplete="off" >
                                    <span class="msg text-danger">${usernameError}</span>
<%--                                    <div class="valid-feedback">验证成功！</div>--%>
<%--                                    <div class="invalid-feedback">--%>
<%--                                    </div>--%>
                                </div>

                                <div class="form-group">
                                    <input id="login-password" type="password" name="password" data-msg="请输入密码"
                                           placeholder="密码" class="input-material" autocomplete="off">
                                    <span class="msg text-danger">${passwordError}</span>
<%--                                    <div class="invalid-feedback">--%>
<%--                                    </div>--%>
                                </div>
                                <div class="form-group " style="margin-bottom:15px;">
                                    <label class="radio-inline">
                                        <input type="radio" name="type" checked value="systemAdmin"
                                               class="radio-inline"> 系统管理员
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="type" value="dormitoryAdmin" class="radio-inline">
                                        宿舍管理员
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary mr-5">登录</button>
                                <button type="reset" class="btn btn-primary " name="submit">重置</button>

                            </form>
                            <br/>
<%--                            <small>没有账号?</small><a href="register.jsp" class="signup">&nbsp;注册</a>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    // 2 验证用户名不为空
    // 2.1 获取用户名表单
    const username = document.querySelector('#login-username')
    // 2.2 使用change事件 值发生变化时触发
    username.addEventListener('change', verifyName)

    // 2.3 封装verifyName函数
    function verifyName() {
        const span = username.nextElementSibling
        // 2.4 定规则 用户名
        if (username.value.trim() === '') {
            span.innerHTML = "请输入用户名"
            // username.classList.remove('is-valid','form-control')
            // username.classList.add('is-invalid','form-control')
            return false
        }
        // 2.5 合法就清空span
        span.innerText = ''
        // username.classList.remove('is-invalid','form-control')
        // username.classList.add('is-valid','form-control')
        return true
    }

    // 3. 验证密码不为空
    // 2.1 获取密码表单
    const password = document.querySelector('#login-password')
    // 2.2 使用change事件 值发生变化时触发
    password.addEventListener('change', verifyPsword)

    // 2.3 封装verifyName函数
    function verifyPsword() {
        const span = password.nextElementSibling
        // 2.4 定规则 用户名
        if (!password.value) {
            span.innerHTML = "请输入密码"
            // password.classList.remove('is-valid','form-control')
            // password.classList.add('is-invalid','form-control')
            return false
        }
        // 2.5 合法就清空span
        span.innerText = ''
        // password.classList.remove('is-invalid','form-control')
        // password.classList.add('is-valid','form-control')
        return true
    }

    // 8. 提交模块
    const form = document.querySelector('#login_form')
    console.log(form);
    form.addEventListener('submit', function (e) {
        // 依次判断上面的每个框框 是否通过，只要有一个没有通过的就阻止
        console.log(verifyName())
        if (!verifyName()) e.preventDefault()
        if (!verifyPsword()) e.preventDefault()
    })

</script>
</body>

</html>