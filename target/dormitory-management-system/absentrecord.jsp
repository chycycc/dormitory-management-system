<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <c:set var="baseurl" value="${pageContext.request.contextPath}"/>
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script type="text/javascript" src="${baseurl}/js/jquery/2.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${baseurl}/bootstrap-3.3.7/css/bootstrap.min.css">
    <script src="${baseurl}/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <!-- 引入 font-awesome -->
    <link rel="stylesheet" href="${baseurl}/font-awesome-4.7.0/css/font-awesome.min.css">
    <title>宿舍管理系统</title>
    <style>
        body {
            margin-bottom: 100px;
        }

        .footer {
            display: flex;
            /* position: fixed; */
            /* bottom: 0; */
            /* left: 0; */
            /* background-color: white; */
            width: 100%;
            height: 100px;
            justify-content: space-between;
        }

        .pageSize {
            display: flex;
            width: 300px;
            align-items: center;
            justify-content: space-around;
        }

        .pageSize label {
            font-size: 18px;
            max-width: 100%;
            margin-bottom: 0;
        }

        .pageSize select {
            width: 30%;
        }

        #pager {
            display: flex;
            align-items: center;
        }

        html, body {
            width: 100%;
            height: 100%;
            overflow-y: scroll;
        }

        ::-webkit-scrollbar {
            display: none;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <!-- 顶部搜索部分 -->
            <div class="panel panel-default">
                <div class="panel-heading">搜索</div>
                <div class="panel-body">
                    <form role="form" class="form-inline" action="/absent/search" method="post">
                        <div class="form-group">
                            <label for="name">字段：</label>
                            <select name="key" class="form-control">
                                <option value="buildingName">楼宇</option>
                                <option value="dormitoryName">宿舍</option>
                            </select>
                        </div>
                        <div class="form-group" style="margin-left: 20px">
                            <label for="value">值：</label>
                            <input type="text" class="form-control" name="value" placeholder="字段值" maxlength="12"
                                   style="width: 130px">
                        </div>
                        <div class="form-group " style="margin-left: 20px">
                            <button type="submit" class="btn btn-info ">
										<span style="margin-right: 5px"
                                              class="glyphicon glyphicon-search" aria-hidden="true">
										</span>开始搜索
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- 列表展示-->
            <div class="table-responsive">
                <table class="table table-hover ">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>楼宇</th>
                        <th>宿舍</th>
                        <th>姓名</th>
                        <th>原因</th>
                        <th>宿管</th>
                        <th>日期</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${list}" var="absent">
                        <tr>
                            <td>${absent.id}</td>
                            <td>${absent.buildingName}</td>
                            <td>${absent.dormitoryName}</td>
                            <td>${absent.studentName}</td>
                            <td>${absent.reason}</td>
                            <td>${absent.dormitoryAdminName}</td>
                            <td>${absent.createDate}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="footer">
    <div class="pageSize">
        <label for="name" id="total">共${total}条数据。 每页</label>
        <select class="form-control" name="pageSize" id="pageSize">
            <option value="10">10</option>
            <option value="15">15</option>
            <option value="20">20</option>
        </select>
    </div>
    <div id="pager">
        <ul class="pagination pagination-lg">
            <li class="home disabled"><a href="#">首页</a></li>
            <li class="previous disabled"><a href="#">&laquo;</a></li>
        </ul>
        <ul id="page" class="pagination pagination-lg">
            <li class="active"><a href="javascript:;">1</a></li>
            <li><a href="javascript:;">2</a></li>
            <li><a href="javascript:;">3</a></li>
            <li><a href="javascript:;">4</a></li>
            <li><a href="javascript:;">5</a></li>
            <li><a href="javascript:;">6</a></li>
            <li><a href="javascript:;">7</a></li>
        </ul>
        <ul class="pagination pagination-lg">
            <li class="next"><a href="#">&raquo;</a></li>
            <li class="last"><a href="#">尾页</a></li>
        </ul>
    </div>
</div>

<script>
    $(function () {
        let i = 1
        // 首页
        const home = document.querySelector(".home")
        // 尾页
        const last = document.querySelector(".last")
        // 下一页
        const next = document.querySelector(".next")
        // 上一页
        const previous = document.querySelector(".previous")
        // 页码
        const ulList = document.querySelector(`#page`)
        // 页数大小
        const pageSize = document.querySelector("#pageSize")

        //声明渲染函数
        function toggle(e) {
            //通过使用 .active 来指示当前的页面
            document.querySelector(`#page .active`).classList.remove('active') //删除
            e.target.parentNode.classList.add('active') //添加
            i = +e.target.innerHTML
            show(i)
        }

        // 上一页下一页显示
        function show(i) {
            if (i === 1) {
                home.classList.add('disabled')
                previous.classList.add('disabled')
                last.classList.remove('disabled')
                next.classList.remove('disabled')
            } else if (i === 7) {
                last.classList.add('disabled')
                next.classList.add('disabled')
                home.classList.remove('disabled')
                previous.classList.remove('disabled')
            } else {
                home.classList.remove('disabled')
                previous.classList.remove('disabled')
                next.classList.remove('disabled')
                last.classList.remove('disabled')
            }
        }

        //首页
        home.addEventListener('click', function (e) {
            i = 1
            show(i)
            document.querySelector(`#page .active`).classList.remove('active')
            document.querySelector("#page li:nth-child(1)").className = 'active'
            const page = 1
            const pagesize = +pageSize.value
            $.ajax({
                url: "http://localhost:8080/absent/page",
                type: "get",
                dataType: "json",
                data: {
                    'page': page,
                    'pageSize': pagesize
                },
                success: function (response) {
                    const data = response[0].rows
                    const total = response[0].total
                    console.log(data);
                    let dataArray = []

                    dataArray = $.map(data, function (item) {
                        let {
                            id,
                            studentName,
                            dormitoryName,
                            buildingName,
                            dormitoryAdminName,
                            reason,
                            createDate
                        } = item

                        return "<tr>" +
                            "<td>" + id + "</td>" +
                            "<td>" + buildingName + "</td>" +
                            "<td>" + dormitoryName + "</td>" +
                            "<td>" + studentName + "</td>" +
                            "<td>" + reason + "</td>" +
                            "<td>" + dormitoryAdminName + "</td>" +
                            "<td>" + createDate + "</td>" +
                            "</tr>"
                    })
                    $("tbody").empty().html(dataArray.join(''))
                    $("#total").empty().html("共" + total + "条数据。 每页")
                }
            })
        })

        //尾页
        last.addEventListener('click', function (e) {
            i = 7
            show(i)
            document.querySelector(`#page .active`).classList.remove('active')
            document.querySelector("#page li:nth-child(7)").className = 'active'
            const page = 7
            const pagesize = +pageSize.value
            $.ajax({
                url: "http://localhost:8080/absent/page",
                type: "get",
                dataType: "json",
                data: {
                    'page': page,
                    'pageSize': pagesize
                },
                success: function (response) {
                    const data = response[0].rows
                    const total = response[0].total
                    console.log(data);
                    let dataArray = []

                    dataArray = $.map(data, function (item) {
                        let {
                            id,
                            studentName,
                            dormitoryName,
                            buildingName,
                            dormitoryAdminName,
                            reason,
                            createDate
                        } = item

                        return "<tr>" +
                            "<td>" + id + "</td>" +
                            "<td>" + buildingName + "</td>" +
                            "<td>" + dormitoryName + "</td>" +
                            "<td>" + studentName + "</td>" +
                            "<td>" + reason + "</td>" +
                            "<td>" + dormitoryAdminName + "</td>" +
                            "<td>" + createDate + "</td>" +
                            "</tr>"
                    })
                    $("tbody").empty().html(dataArray.join(''))
                    $("#total").empty().html("共" + total + "条数据。 每页")
                }
            })
        })

        // 上一个
        previous.addEventListener('click', function (e) {
            i = +document.querySelector(`#page .active`).childNodes[0].innerHTML
            console.log(i);
            show(i - 1)
            let index = i - 1
            <%--document.querySelector(`#page li:nth-child(${i - 1})`).className = 'active'--%>
            <%--document.querySelector(`#page li:nth-child(${i})`).className = ''--%>
            document.querySelector("#page li:nth-child(" + index + ")").className = 'active'
            document.querySelector("#page li:nth-child(" + i + ")").className = ''

            const page = i - 1
            const pagesize = +pageSize.value
            $.ajax({
                url: "http://localhost:8080/absent/page",
                type: "get",
                dataType: "json",
                data: {
                    'page': page,
                    'pageSize': pagesize
                },
                success: function (response) {
                    const data = response[0].rows
                    const total = response[0].total
                    console.log(data);
                    let dataArray = []

                    dataArray = $.map(data, function (item) {
                        let {
                            id,
                            studentName,
                            dormitoryName,
                            buildingName,
                            dormitoryAdminName,
                            reason,
                            createDate
                        } = item

                        return "<tr>" +
                            "<td>" + id + "</td>" +
                            "<td>" + buildingName + "</td>" +
                            "<td>" + dormitoryName + "</td>" +
                            "<td>" + studentName + "</td>" +
                            "<td>" + reason + "</td>" +
                            "<td>" + dormitoryAdminName + "</td>" +
                            "<td>" + createDate + "</td>" +
                            "</tr>"
                    })
                    $("tbody").empty().html(dataArray.join(''))
                    $("#total").empty().html("共" + total + "条数据。 每页")
                }
            })
        })
        // 下一个
        next.addEventListener('click', function (e) {
            i = +document.querySelector(`#page .active`).childNodes[0].innerHTML
            console.log(i);
            show(i + 1)
            document.querySelector(`#page .active`).nextElementSibling.classList.add('active')
            document.querySelector(`#page .active`).classList.remove('active')
            const page = i + 1
            const pagesize = +pageSize.value
            console.log(`${pagesize}-------${page}`);
            $.ajax({
                url: "http://localhost:8080/absent/page",
                type: "get",
                dataType: "json",
                data: {
                    'page': page,
                    'pageSize': pagesize
                },
                success: function (response) {
                    const data = response[0].rows
                    const total = response[0].total
                    console.log(data);
                    let dataArray = []

                    dataArray = $.map(data, function (item) {
                        let {
                            id,
                            studentName,
                            dormitoryName,
                            buildingName,
                            dormitoryAdminName,
                            reason,
                            createDate
                        } = item

                        return "<tr>" +
                            "<td>" + id + "</td>" +
                            "<td>" + buildingName + "</td>" +
                            "<td>" + dormitoryName + "</td>" +
                            "<td>" + studentName + "</td>" +
                            "<td>" + reason + "</td>" +
                            "<td>" + dormitoryAdminName + "</td>" +
                            "<td>" + createDate + "</td>" +
                            "</tr>"
                    })
                    $("tbody").empty().html(dataArray.join(''))
                    $("#total").empty().html("共" + total + "条数据。 每页")
                }
            })
        })
        // 页数
        ulList.addEventListener('click', function (e) {
            toggle(e)
            const page = +e.target.innerHTML
            const pagesize = +pageSize.value
            console.log(`${pagesize}-------${page}`);
            $.ajax({
                url: "http://localhost:8080/absent/page",
                type: "get",
                dataType: "json",
                data: {
                    'page': page,
                    'pageSize': pagesize
                },
                success: function (response) {
                    const data = response[0].rows
                    const total = response[0].total
                    console.log(data);
                    let dataArray = []

                    dataArray = $.map(data, function (item) {
                        let {
                            id,
                            studentName,
                            dormitoryName,
                            buildingName,
                            dormitoryAdminName,
                            reason,
                            createDate
                        } = item

                        return "<tr>" +
                            "<td>" + id + "</td>" +
                            "<td>" + buildingName + "</td>" +
                            "<td>" + dormitoryName + "</td>" +
                            "<td>" + studentName + "</td>" +
                            "<td>" + reason + "</td>" +
                            "<td>" + dormitoryAdminName + "</td>" +
                            "<td>" + createDate + "</td>" +
                            "</tr>"
                    })
                    $("tbody").empty().html(dataArray.join(''))
                    $("#total").empty().html("共" + total + "条数据。 每页")
                }
            })
        })

    });
</script>
</body>

</html>